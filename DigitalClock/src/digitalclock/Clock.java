package digitalclock;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Observable;
import java.util.Timer;
import java.util.TimerTask;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 * A simple alarm clock which is a singleton. Needs ClockUI to operate properly.
 * 
 * @author Benjapol Worakan 5710546577
 * @version 15.4.9
 */
public class Clock extends Observable {
	/** A singleton instance of the Clock. */
	private static final Clock INSTANCE = new Clock();
	/**
	 * Visibility states of each elements on the Clock's face, used to make
	 * elements blinkable.
	 */
	private boolean[] visibilityStates;
	/** Hour, minute, and second that is displaying on the Clock's face. */
	private int displayHour, displayMinute, displaySecond;
	/** Hour, minute, and second that the alarm is set to sound on. */
	private int alarmHour, alarmMinute, alarmSecond;
	/** Alarm sound clip. */
	private Clip clip;
	/** A Timer that will execute clockTickerTask every second. */
	private Timer timer;
	/** A TimerTask that will update the clock's face and sound the alarm. */
	private TimerTask clockTickerTask;
	/** Alarm state of the Clock, will sound alarm only when ON. */
	private AlarmState alarmState;
	/** State of the Clock (showing time or setting alarm). */
	private ClockState clockState;

	/**
	 * State of the virtual alarm switch of the clock.
	 * 
	 * @author Benjapol Worakan 5710546577
	 * @version 15.4.9
	 */
	private enum AlarmState {
		ON {

			@Override
			public void activateAlarm() {
				Clock.getInstance().clip.stop();
				Clock.getInstance().clip.setMicrosecondPosition(2 * (long) Math
						.pow(10, 6));
				Clock.getInstance().clip.start();
			}

			@Override
			public void toggleAlarm() {
				Clock.getInstance().clip.stop();
				Clock.getInstance().alarmState = OFF;
			}

		},
		OFF {

			@Override
			public void activateAlarm() {
				Clock.getInstance().clip.stop();
			}

			@Override
			public void toggleAlarm() {
				Clock.getInstance().clip.stop();
				Clock.getInstance().alarmState = ON;
			}

		};
		/**
		 * Sound the alarm.
		 */
		public abstract void activateAlarm();

		/**
		 * Toggle the virtual alarm switch on or off.
		 */
		public abstract void toggleAlarm();
	}

	/**
	 * State of the clock's face.
	 * 
	 * @author Benjapol Worakan
	 * @version 15.4.9
	 */
	private enum ClockState {
		DISPLAY_TIME {
			@Override
			public void handleSetButton() {
				Clock.getInstance().clockState = SET_ALARM_HOUR;

				Clock.getInstance().visibilityStates[1] = true;
				Clock.getInstance().visibilityStates[3] = true;
				Clock.getInstance().visibilityStates[0] = false;
			}

			@Override
			public void handleAddButton() {
				// Do nothing.
			}

			@Override
			public void handleSubtractButton() {
				// Do nothing.
			}

			@Override
			public void updateDisplay() {
				Clock.getInstance().displayHour = Calendar.getInstance().get(
						Calendar.HOUR_OF_DAY);
				Clock.getInstance().displayMinute = Calendar.getInstance().get(
						Calendar.MINUTE);
				Clock.getInstance().displaySecond = Calendar.getInstance().get(
						Calendar.SECOND);
			}
		},
		SET_ALARM_HOUR {

			@Override
			public void handleSetButton() {
				Clock.getInstance().clockState = SET_ALARM_MINUTE;

				Clock.getInstance().visibilityStates[0] = true;
				Clock.getInstance().visibilityStates[2] = false;
			}

			@Override
			public void handleAddButton() {
				Clock.getInstance().alarmHour++;
			}

			@Override
			public void handleSubtractButton() {
				Clock.getInstance().alarmHour--;
			}

			@Override
			public void updateDisplay() {
				updateSetAlarmModeDisplay();
			}

		},
		SET_ALARM_MINUTE {

			@Override
			public void handleSetButton() {
				Clock.getInstance().clockState = SET_ALARM_SECOND;

				Clock.getInstance().visibilityStates[2] = true;
				Clock.getInstance().visibilityStates[4] = false;
			}

			@Override
			public void handleAddButton() {
				Clock.getInstance().alarmMinute++;
			}

			@Override
			public void handleSubtractButton() {
				Clock.getInstance().alarmMinute--;
			}

			@Override
			public void updateDisplay() {
				updateSetAlarmModeDisplay();
			}

		},
		SET_ALARM_SECOND {

			@Override
			public void handleSetButton() {
				Clock.getInstance().clockState = DISPLAY_TIME;

				Clock.getInstance().visibilityStates[4] = true;
				Clock.getInstance().visibilityStates[1] = false;
				Clock.getInstance().visibilityStates[3] = false;
			}

			@Override
			public void handleAddButton() {
				Clock.getInstance().alarmSecond++;
			}

			@Override
			public void handleSubtractButton() {
				Clock.getInstance().alarmSecond--;
			}

			@Override
			public void updateDisplay() {
				updateSetAlarmModeDisplay();
			}

		};

		/**
		 * Handle actions of the SET button on GUI.
		 */
		public abstract void handleSetButton();

		/**
		 * Handle actions of the + button on GUI.
		 */
		public abstract void handleAddButton();

		/**
		 * Handle actions of the - button on GUI.
		 */
		public abstract void handleSubtractButton();

		/**
		 * Update the clock's face.
		 */
		public abstract void updateDisplay();

		/**
		 * Update the clock's face when in any of the set alarm mode.
		 */
		public void updateSetAlarmModeDisplay() {
			if (Clock.getInstance().alarmHour >= 24) {
				Clock.getInstance().alarmHour -= 24;
			} else if (Clock.getInstance().alarmHour < 0) {
				Clock.getInstance().alarmHour = 23;
			}

			if (Clock.getInstance().alarmMinute >= 60) {
				Clock.getInstance().alarmMinute -= 60;
			} else if (Clock.getInstance().alarmMinute < 0) {
				Clock.getInstance().alarmMinute = 59;
			}

			if (Clock.getInstance().alarmSecond >= 60) {
				Clock.getInstance().alarmSecond -= 60;
			} else if (Clock.getInstance().alarmSecond < 0) {
				Clock.getInstance().alarmSecond = 59;
			}

			Clock.getInstance().displayHour = Clock.getInstance().alarmHour;
			Clock.getInstance().displayMinute = Clock.getInstance().alarmMinute;
			Clock.getInstance().displaySecond = Clock.getInstance().alarmSecond;
		}
	}

	/**
	 * Private constructor of the Clock. Will initializes all elements
	 * requisites.
	 */
	private Clock() {
		visibilityStates = new boolean[] { true, true, true, true, true };
		displayHour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
		displayMinute = Calendar.getInstance().get(Calendar.MINUTE);
		displaySecond = Calendar.getInstance().get(Calendar.SECOND);
		alarmHour = 0;
		alarmMinute = 0;
		alarmSecond = 0;
		clockState = ClockState.DISPLAY_TIME;
		alarmState = AlarmState.OFF;
		timer = new Timer();
		clockTickerTask = new TimerTask() {
			@Override
			public void run() {
				tick();
			}
		};
		timer.scheduleAtFixedRate(clockTickerTask,
				1000 - System.currentTimeMillis() % 1000, 500);

		try {
			clip = AudioSystem.getClip();
			clip.open(AudioSystem.getAudioInputStream(new File(
					"./assets/MorningWalk.wav")));
		} catch (LineUnavailableException | IOException
				| UnsupportedAudioFileException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Tick the clock every second. Also responsible for sounding the alarm.
	 */
	private void tick() {
		clockState.updateDisplay();
		setChanged();
		notifyObservers(visibilityStates);
		if (Calendar.getInstance().get(Calendar.HOUR_OF_DAY) == alarmHour
				&& Calendar.getInstance().get(Calendar.MINUTE) == alarmMinute
				&& Calendar.getInstance().get(Calendar.SECOND) == alarmSecond) {
			alarmState.activateAlarm();
		}
	}

	/**
	 * Get a singleton instance of the Clock.
	 * 
	 * @return A singleton instance of the Clock.
	 */
	public static Clock getInstance() {
		return INSTANCE;
	}

	/**
	 * Get value of hour to be displayed on the Clock's face.
	 * 
	 * @return Value of hour to be displayed.
	 */
	public int getDisplayHour() {
		return displayHour;
	}

	/**
	 * Get value of minute to be displayed on the Clock's face.
	 * 
	 * @return Value of minute to be displayed.
	 */
	public int getDisplayMinute() {
		return displayMinute;
	}

	/**
	 * Get value of second to be displayed on the Clock's face.
	 * 
	 * @return Value of second to be displayed.
	 */
	public int getDisplaySecond() {
		return displaySecond;
	}

	/**
	 * Get current status of the virtual alarm switch (on or off).
	 * 
	 * @return Status of the virtual alarm switch (on or off).
	 */
	public String getAlarmStatus() {
		return alarmState.name();
	}

	/**
	 * Handle actions of the SET button on GUI.
	 */
	public void handleSetButton() {
		clockState.handleSetButton();
		tick();
	}

	/**
	 * Handle actions of the + button on GUI.
	 */
	public void handleAddButton() {
		clockState.handleAddButton();
		tick();
	}

	/**
	 * Handle actions of the - button on GUI.
	 */
	public void handleSubtractButton() {
		clockState.handleSubtractButton();
		tick();
	}

	/**
	 * Handle actions of the alarm toggle button on GUI.
	 */
	public void handleAlarmToggle() {
		alarmState.toggleAlarm();
		tick();
	}
}
