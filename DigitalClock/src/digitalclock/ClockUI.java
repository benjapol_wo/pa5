package digitalclock;

import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

/**
 * GUI that is used for observing the Clock.
 * 
 * @author Benjapol Worakan 5710546577
 * @version 15.4.9
 */
public class ClockUI extends JFrame implements Runnable, Observer {
	/** Serial version UID. */
	private static final long serialVersionUID = 1L;
	/** JLabels for displaying time and information from Clock. */
	private JLabel hour, minute, second, separator_1, separator_2;
	/** Alarm toggle on-off switch. */
	private JButton alarmToggle;
	/** A timer that will flash specified elements on the Clock's face. */
	private Timer timer;
	/** Visibility state of each elements on the Clock's face. */
	private boolean[] visibilityStates;
	/**
	 * Current visibility of elements that have their visibilityState equals to
	 * false.
	 */
	private boolean phased;

	/**
	 * Constructor of ClockUI.
	 */
	public ClockUI() {
		super("Digital Clock");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		initComponents();
	}

	/**
	 * Run the GUI.
	 */
	@Override
	public void run() {
		pack();
		setVisible(true);
		timer.start();
	}

	/**
	 * Initialize GUI components of ClockUI.
	 */
	private void initComponents() {
		Container contentPane = getContentPane();
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));

		JPanel displayPanel = new JPanel(), controlPanel = new JPanel();
		contentPane.add(displayPanel);
		contentPane.add(controlPanel);

		displayPanel.setLayout(new BoxLayout(displayPanel, BoxLayout.X_AXIS));
		controlPanel.setLayout(new BoxLayout(controlPanel, BoxLayout.X_AXIS));

		int fontSize = 60;
		String fontName = "System";

		hour = new JLabel("00");
		hour.setFont(new Font(fontName, Font.BOLD, fontSize));
		separator_1 = new JLabel(":");
		separator_1.setFont(new Font(fontName, Font.BOLD, fontSize));
		minute = new JLabel("00");
		minute.setFont(new Font(fontName, Font.BOLD, fontSize));
		separator_2 = new JLabel(":");
		separator_2.setFont(new Font(fontName, Font.BOLD, fontSize));
		second = new JLabel("00");
		second.setFont(new Font(fontName, Font.BOLD, fontSize));

		displayPanel.add(hour);
		displayPanel.add(separator_1);
		displayPanel.add(minute);
		displayPanel.add(separator_2);
		displayPanel.add(second);

		JButton setButton = new JButton("SET"), addButton = new JButton("+"), subtractButton = new JButton(
				"-");
		alarmToggle = new JButton("OFF");

		controlPanel.add(setButton);
		controlPanel.add(addButton);
		controlPanel.add(subtractButton);
		controlPanel.add(alarmToggle);

		setButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Clock.getInstance().handleSetButton();
			}

		});

		addButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Clock.getInstance().handleAddButton();
			}

		});

		subtractButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Clock.getInstance().handleSubtractButton();
			}

		});

		alarmToggle.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Clock.getInstance().handleAlarmToggle();
			}

		});

		visibilityStates = new boolean[5];

		timer = new Timer(500, new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (!visibilityStates[0] && phased) {
					hour.setForeground(contentPane.getBackground());
				} else {
					hour.setForeground(contentPane.getForeground());
				}

				if (!visibilityStates[1] && phased) {
					separator_1.setForeground(contentPane.getBackground());
				} else {
					separator_1.setForeground(contentPane.getForeground());
				}

				if (!visibilityStates[2] && phased) {
					minute.setForeground(contentPane.getBackground());
				} else {
					minute.setForeground(contentPane.getForeground());
				}

				if (!visibilityStates[3] && phased) {
					separator_2.setForeground(contentPane.getBackground());
				} else {
					separator_2.setForeground(contentPane.getForeground());
				}

				if (!visibilityStates[4] && phased) {
					second.setForeground(contentPane.getBackground());
				} else {
					second.setForeground(contentPane.getForeground());
				}

				if (phased) {
					phased = false;
				} else {
					phased = true;
				}
			}

		});
	}

	/**
	 * Update the clock's face.
	 * 
	 * @param clockObject
	 *            - the Clock
	 * @param visibilityStates
	 *            - visibility state of each items on the Clock's face
	 */
	@Override
	public void update(Observable clockObject, Object visibilityStates) {
		if (clockObject instanceof Clock) {
			Clock clock = (Clock) clockObject;
			setDisplay(clock.getDisplayHour(), clock.getDisplayMinute(),
					clock.getDisplaySecond(), clock.getAlarmStatus());
		}
		if (visibilityStates instanceof boolean[]) {
			this.visibilityStates = (boolean[]) visibilityStates;
		}
	}

	private void setDisplay(int hour, int minute, int second, String alarmStatus) {
		this.hour.setText(String.format("%02d", hour));
		this.minute.setText(String.format("%02d", minute));
		this.second.setText(String.format("%02d", second));
		this.alarmToggle.setText(alarmStatus);
	}

}
