package digitalclock;

import java.awt.EventQueue;

public class Main {
	public static void main(String[] args) {
		ClockUI clockUI = new ClockUI();
		Clock.getInstance().addObserver(clockUI);
		EventQueue.invokeLater(clockUI);
	}
}
